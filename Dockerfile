FROM python:3-buster

# clone source
WORKDIR /usr/src/
RUN git clone https://github.com/Taxel/PlexTraktSync

# persist /data
VOLUME /data

# change to source install deps
WORKDIR /usr/src/PlexTraktSync
RUN pip3 install -r requirements.txt

# copy source to docker
# ADD PlexTraktSync/ /data/

# change to data

# WORKDIR /data

# add cronjob https://stackoverflow.com/a/37458519

RUN apt-get update && apt-get -y install cron

# Copy cron file to the cron.d directory
COPY plexsync-cron /etc/cron.d/plexsync-cron

# Give execution rights on the cron job
RUN chmod 0644 /etc/cron.d/plexsync-cron

# If you're adding a script file and telling cron to run it, remember to # cron fails silently if you forget
RUN chmod 0744 /data/plex_trakt_sync.sh

# Apply cron job
RUN crontab /etc/cron.d/plexsync-cron

# Create the log file to be able to run tail
RUN touch /var/log/cron.log

# Run the command on container startup
CMD cron && tail -f /var/log/cron.log && write-env.sh
